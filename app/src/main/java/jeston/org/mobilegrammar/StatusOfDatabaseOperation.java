package jeston.org.mobilegrammar;

/**
 * Created by Jeston on 28.06.2016.
 */
public enum StatusOfDatabaseOperation {
    NEW, UPDATE
}
