package jeston.org.mobilegrammar;

/**
 * Created by Jeston on 30.06.2016.
 */
public enum ActivityArticlesStatusToShow {
    SHOW_ALL_ARTICLES, SHOW_ALL_GROUPS
}
